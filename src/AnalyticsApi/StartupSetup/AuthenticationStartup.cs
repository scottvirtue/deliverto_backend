﻿using Microsoft.Extensions.DependencyInjection;

namespace AnalyticsApi.StartupSetup
{
    public static class AuthenticationStartup
    {
        public static IServiceCollection AddAuthenticationService(this IServiceCollection services, Microsoft.Extensions.Configuration.IConfiguration configuration)
        {
            var poolClientId = configuration["PoolClientId"];
            var poolAuthorityUrl = configuration["PoolAuthorityUrl"];

            services.AddAuthentication("Bearer").AddJwtBearer(x =>
            {
                x.Audience = poolClientId;
                x.Authority = poolAuthorityUrl;
                x.RequireHttpsMetadata = false;
            });

            return services;
        }
    }
}
