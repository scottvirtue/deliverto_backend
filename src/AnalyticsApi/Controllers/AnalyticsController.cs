using System.Threading.Tasks;
using Domain.Services.Analytics;
using Microsoft.AspNetCore.Mvc;

namespace AnalyticsApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AnalyticsController : ControllerBase
    {
        private readonly IAnalyticsService _analyticsService;

        public AnalyticsController(IAnalyticsService analyticsService)
        {
            _analyticsService = analyticsService;
        }

        [HttpGet("users/list")]
        public async Task<IActionResult> ListUsers()
        {
            var result = await _analyticsService.ListUsers();

            return Ok(result);
        }

        [HttpGet("deliveries/list")]
        public async Task<IActionResult> ListDeliveries()
        {
            var result = await _analyticsService.ListDeliveries();

            return Ok(result);
        }
    }
}
