﻿using System.Security.Principal;
using Amazon.DynamoDBv2;
using Amazon.Lambda.Core;
using Amazon.SimpleNotificationService;
using AutoMapper;
using Domain.DataAccess;
using Domain.Mapper;
using Domain.Models;
using Domain.Services;
using Domain.Services.Analytics;
using Domain.Services.Analytics.Command.DeliveryCommandTypes;
using Domain.Services.Analytics.Command.UserCommandTypes;
using Domain.Services.Analytics.Relational;
using Domain.Services.Analytics.Repository.Delivery;
using Domain.Services.Analytics.Repository.UnitOfWork;
using Domain.Services.Analytics.Repository.User;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Domain.IoC
{
    public static class Bindings
    {
        public static void ConfigureIoC(this IServiceCollection services, IConfiguration configuration, ILambdaContext lambdaContext = null)
        {
            services.AddAWSService<IAmazonSimpleNotificationService>();
            services.AddAWSService<Amazon.DynamoDBv2.IAmazonDynamoDB>();

            services.AddAutoMapper(typeof(DomainMappingProfile));
            services.AddTransient<IDeliveriesService, DeliveriesService>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IAuthenticationService>(x =>
                new AuthenticationService(lambdaContext == null ? x.GetService<IPrincipal>() : null));

            if (!string.IsNullOrEmpty(configuration["AnalyticsDatabaseConnectionString"]))
            {
                services.AddDbContext<AnalyticsDbContext>(op => op.UseMySQL(configuration["AnalyticsDatabaseConnectionString"]),
                                                         ServiceLifetime.Transient);

                services.AddTransient<IAnalyticsService, AnalyticsService>();
                services.AddTransient<IDeliveryRepository, DeliveryRepository>();
                services.AddTransient<IUserRepository, UserRepository>();
                services.AddTransient<IUnitOfWork, UnitOfWork>();
                services.AddTransient<IDeliveryCommandType, UpsertDeliveryCommand>();
                services.AddTransient<IUserCommandTypes, UpsertUserCommand>();
            }
          
            if (!string.IsNullOrEmpty(configuration["UsersTable"]))
            {
                services.AddTransient<IDataAccess<UserModel, UserModel>, DataAccess<UserModel, UserModel>>();
                services.AddTransient<IDynamoTable<UserModel>>(x =>
                    new DynamoTable<UserModel>(x.GetService<IAmazonDynamoDB>(),
                        tableName: configuration["UsersTable"]));
            }

            if (!string.IsNullOrEmpty(configuration["DeliveriesTable"]))
            {
                services.AddTransient<IDataAccess<DeliveryModel, DeliveryModel>, DataAccess<DeliveryModel, DeliveryModel>>();
                services.AddTransient<IDynamoTable<DeliveryModel>>(x =>
                    new DynamoTable<DeliveryModel>(x.GetService<IAmazonDynamoDB>(),
                        tableName: configuration["DeliveriesTable"]));
            }

            if (lambdaContext != null)
            {
                services.AddSingleton(provider => configuration);
            }
        }
    }
}
