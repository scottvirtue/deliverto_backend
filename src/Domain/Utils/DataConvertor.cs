﻿using Amazon.DynamoDBv2.DocumentModel;
using Newtonsoft.Json;

namespace Domain.Utils
{
    public static class DataConvertor
    {
        public static Document ToDoc<TModel>(this TModel model)
        {
            var json = JsonConvert.SerializeObject(model, Newtonsoft.Json.Formatting.Indented,
                new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

            return Document.FromJson(json);
        }

        public static T ToModel<T>(this Document doc)
        {
            var json = doc.ToJson();
            var model = JsonConvert.DeserializeObject<T>(json);

            return model;
        }
    }
}
