﻿using AutoMapper;
using Domain.Models;
using Domain.Services.Analytics.Relational.Entities;
using Newtonsoft.Json;

namespace Domain.Mapper
{
    public class DomainMappingProfile : Profile
    {
        public DomainMappingProfile()
        {
            CreateMissingTypeMaps = true;

            CreateMap<DeliveryModel, Delivery>().AfterMap((source, dest) =>
            {
                dest.Buyer = JsonConvert.SerializeObject(source.Buyer);
                dest.Receiver = JsonConvert.SerializeObject(source.Receiver);
            });

            CreateMap<Delivery, DeliveryModel>().ForMember(x => x.Buyer, opt => opt.Ignore())
                                                .ForMember(x => x.Receiver, opt => opt.Ignore()).AfterMap((source, dest) =>
            {
                if (!string.IsNullOrEmpty(source.Buyer))
                {
                    dest.Buyer = JsonConvert.DeserializeObject<UserModel>(source.Buyer);
                }
                if (!string.IsNullOrEmpty(source.Receiver))
                {
                    dest.Receiver = JsonConvert.DeserializeObject<UserModel>(source.Receiver);
                }
            });

            CreateMap<UserModel, User>().AfterMap((source, dest) =>
            {
                dest.Id = source.Username;
                dest.Location = JsonConvert.SerializeObject(source.Location);
            });

            CreateMap<User, UserModel>().ForMember(x => x.Location, opt => opt.Ignore()).AfterMap((source, dest) =>
            {
                dest.Username = source.Id;
                dest.Location = JsonConvert.DeserializeObject<Location>(source.Location);
            });
        }
    }
}
