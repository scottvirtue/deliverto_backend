﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Amazon.DynamoDBv2.DocumentModel;

namespace Domain.DataAccess
{
    public interface IDynamoTable<TDataAccessModel>
    {
        Task<Document> GetItemAsync(string primaryKey, GetItemOperationConfig config);
        Task PutItemAsync(Document doc, PutItemOperationConfig config);
        Task UpdateItemAsync(Document doc, UpdateItemOperationConfig config);
        Task<List<Document>> Query(QueryOperationConfig config);
    }
}