﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.DataAccess
{
    public interface IDataAccess<TDataAccessModel, TModel>
    {
        Task<TModel> Get(string primaryKey);
        Task<List<TModel>> GetMany(string primaryKeyPropertyName, string primaryKey);
        Task Create(TModel model);
        Task Update(TModel model);
        Task<List<TModel>> GetByIndex(string attribute, string value);
    }
}