﻿using System;

namespace Domain.DataAccess.Models
{
    public class AttributeExistsOnUpdate : Attribute {}
}
