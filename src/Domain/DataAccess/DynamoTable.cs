﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;

namespace Domain.DataAccess
{
    public class DynamoTable<TDataAccessModel> : IDynamoTable<TDataAccessModel>
    {
        private readonly IAmazonDynamoDB _client;
        private readonly string _tableName;
        private static Table _table;

        private Table Table
        {
            get
            {
                if (_table != null) return _table;
                _table = Table.LoadTable(_client, _tableName);
                return _table;
            }
        }

        public DynamoTable(IAmazonDynamoDB client, string tableName)
        {
            _client = client;
            _tableName = tableName;
        }

        public async Task<Document> GetItemAsync(string primaryKey, GetItemOperationConfig config)
        {
           return await Table.GetItemAsync(primaryKey, config);
        }

        public async Task PutItemAsync(Document doc, PutItemOperationConfig config)
        {
           await Table.PutItemAsync(doc, config);
        }

        public async Task UpdateItemAsync(Document doc, UpdateItemOperationConfig config)
        {
            await Table.UpdateItemAsync(doc, config);
        }

        public async Task<List<Document>> Query(QueryOperationConfig config)
        {
            var search = Table.Query(config);

            var result = await search.GetNextSetAsync();

            var remainingData = await search.GetRemainingAsync();

            if (remainingData != null && remainingData.Any())
            {
                result.AddRange(remainingData);
            }

            return result;
        }
    }
}
