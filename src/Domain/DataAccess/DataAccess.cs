﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.DynamoDBv2.DocumentModel;
using AutoMapper;
using Domain.DataAccess.Models;
using Domain.Utils;

namespace Domain.DataAccess
{
    public class DataAccess<TDataAccessModel, TModel> : IDataAccess<TDataAccessModel, TModel>
    {
        private readonly IMapper _mapper;
        private readonly IDynamoTable<TDataAccessModel> _dynamoTable;
     
        public DataAccess(IDynamoTable<TDataAccessModel> dynamoTable, IMapper mapper)
        {
            _dynamoTable = dynamoTable;
            _mapper = mapper;
        }

        public async Task<TModel> Get(string primaryKey)
        {
            var config = new GetItemOperationConfig
            {
                ConsistentRead = true
            };

            var doc = await _dynamoTable.GetItemAsync(primaryKey, config);

            return doc == null ? default(TModel) : _mapper.Map<TModel>(doc.ToModel<TDataAccessModel>());
        }

        public async Task<List<TModel>> GetMany(string primaryKeyPropertyName, string primaryKey)
        {
            var filter = new QueryFilter(primaryKeyPropertyName, QueryOperator.Equal, primaryKey);
            
            var config = new QueryOperationConfig
            {
                Filter = filter,
            };

            var data = await _dynamoTable.Query(config);

            return data?.Select(x => x.ToModel<TModel>()).ToList();
        }

        public async Task Create(TModel model)
        {
            var dataAccessModel = _mapper.Map<TDataAccessModel>(model);

            var config = new PutItemOperationConfig
            {
                ConditionalExpression = new Expression
                {
                    ExpressionStatement = BuildExpressionStatement<PutItemOperationConfig>()
                }
            };

            var doc = dataAccessModel.ToDoc();

            await _dynamoTable.PutItemAsync(doc, config);
        }

        public async Task Update(TModel model)
        {
            var dataAccessModel = _mapper.Map<TDataAccessModel>(model);

            var config = new UpdateItemOperationConfig
            {
                ConditionalExpression = new Expression
                {
                    ExpressionStatement = BuildExpressionStatement<UpdateItemOperationConfig>()
                }
            };

            var doc = dataAccessModel.ToDoc();

            await _dynamoTable.UpdateItemAsync(doc, config);
        }

        public async Task<List<TModel>> GetByIndex(string attribute, string value)
        {
            var filter = new QueryFilter(attribute, QueryOperator.Equal, value);

            var config = new QueryOperationConfig
            {
                Filter = filter,
                IndexName = $"By{attribute}"
            };

            var data = await _dynamoTable.Query(config);

            return data?.Select(x => x.ToModel<TModel>()).ToList();
        }

        #region Helpers

        private string BuildExpressionStatement<TIConditionalOperationConfig>()
        {
            void BuildExpressionStatementHelper(StringBuilder builder, string _conditionalStatement, string _propertyName)
            {
                builder.Append($"{(builder.Length > 0 ? " and " : "")}{_conditionalStatement}({_propertyName})");
            }

            var conditionalStatement = "";

            if (typeof(TIConditionalOperationConfig) == typeof(PutItemOperationConfig))
            {
                conditionalStatement = "attribute_not_exists";
            }
            else if (typeof(TIConditionalOperationConfig) == typeof(UpdateItemOperationConfig))
            {
                conditionalStatement = "attribute_exists";
            }
            else
            {
                return "";
            }

            var type = typeof(TDataAccessModel);
            var properties = type.GetProperties();

            var expressionStatementBuilder = new StringBuilder();

            foreach (var property in properties)
            {
                var propertyName = property.Name;

                if (typeof(TIConditionalOperationConfig) == typeof(PutItemOperationConfig))
                {
                    var hasAttribute = Attribute.IsDefined(property, typeof(AttributeNotExistsOnPut));

                    if (hasAttribute)
                    {
                        BuildExpressionStatementHelper(expressionStatementBuilder, conditionalStatement, propertyName);
                    }
                }
                else if (typeof(TIConditionalOperationConfig) == typeof(UpdateItemOperationConfig))
                {
                    var hasAttribute = Attribute.IsDefined(property, typeof(AttributeExistsOnUpdate));

                    if (hasAttribute)
                    {
                        BuildExpressionStatementHelper(expressionStatementBuilder, conditionalStatement, propertyName);
                    }
                }
            }

            return expressionStatementBuilder.ToString();
        }

        #endregion
    }
}
