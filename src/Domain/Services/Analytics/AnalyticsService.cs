﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.Lambda.DynamoDBEvents;
using AutoMapper;
using Domain.Models;
using Domain.Services.Analytics.Command.DeliveryCommandTypes;
using Domain.Services.Analytics.Command.UserCommandTypes;
using Domain.Services.Analytics.Repository.UnitOfWork;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace Domain.Services.Analytics
{
    public interface IAnalyticsService
    {
        Task<bool> UpsertDeliveries(DynamoDBEvent @event);
        Task<bool> UpsertUsers(DynamoDBEvent @event);
        Task<List<DeliveryModel>> ListDeliveries();
        Task<List<UserModel>> ListUsers();
    }

    public class AnalyticsService : IAnalyticsService
    {
        private readonly IEnumerable<IDeliveryCommandType> _deliveryCommandTypes;
        private readonly IEnumerable<IUserCommandTypes> _userCommandTypes;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public AnalyticsService(IEnumerable<IDeliveryCommandType> deliveryCommandTypes,
                                IEnumerable<IUserCommandTypes> userCommandTypes,
                                IUnitOfWork unitOfWork, 
                                IMapper mapper)
        {
            _deliveryCommandTypes = deliveryCommandTypes;
            _userCommandTypes = userCommandTypes;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<bool> UpsertDeliveries(DynamoDBEvent @event)
        {
            var events = new List<UpsertDeliveryEvent>();

            @event.Records.ToList().ForEach(record =>
            {
                var currentEvent = new UpsertDeliveryEvent
                {
                    CommandType = record.EventName,
                    Model = JsonConvert.DeserializeObject<DeliveryModel>(Document.FromAttributeMap(record.Dynamodb.NewImage).ToJson())
                };

                var eventAlreadyExists = events.FirstOrDefault(x => x.Model.Id == currentEvent.Model.Id);

                if (eventAlreadyExists == null)
                {
                    events.Add(currentEvent);
                }
                else
                {
                    if (eventAlreadyExists.Model.ModifiedAt < currentEvent.Model.ModifiedAt)
                    {
                        events.Remove(eventAlreadyExists);
                        events.Add(currentEvent);
                    }
                }
            });

            events.ForEach(upsertDeliveryEvent =>
            {
                _deliveryCommandTypes.FirstOrDefault(x => x.IsType(upsertDeliveryEvent.CommandType))?.Execute(_unitOfWork, upsertDeliveryEvent.Model);
            });

            var result = await _unitOfWork.CompleteAsync();

            return true;
        }

        public async Task<bool> UpsertUsers(DynamoDBEvent @event)
        {
            var events = new List<UpsertUserEvent>();

            @event.Records.ToList().ForEach(record =>
            {
                var currentEvent = new UpsertUserEvent
                {
                    CommandType = record.EventName,
                    Model = JsonConvert.DeserializeObject<UserModel>(Document.FromAttributeMap(record.Dynamodb.NewImage).ToJson())
                };

                var eventAlreadyExists = events.FirstOrDefault(x => x.Model.Username == currentEvent.Model.Username);

                if (eventAlreadyExists == null)
                {
                    events.Add(currentEvent);
                }
                else
                {
                    if (eventAlreadyExists.Model.ModifiedAt < currentEvent.Model.ModifiedAt)
                    {
                        events.Remove(eventAlreadyExists);
                        events.Add(currentEvent);
                    }
                }
            });

            events.ForEach(upsertUserEvent =>
            {
                _userCommandTypes.FirstOrDefault(x => x.IsType(upsertUserEvent.CommandType))?.Execute(_unitOfWork, upsertUserEvent.Model);
            });

            var result = await _unitOfWork.CompleteAsync();

            return true;
        }

        public async Task<List<DeliveryModel>> ListDeliveries()
        {
            var deliveries = await _unitOfWork.DeliveryRepository.GetAll().ToListAsync();

            return _mapper.Map<List<DeliveryModel>>(deliveries.OrderByDescending(x => x.DeliveryDate));
        }

        public async Task<List<UserModel>> ListUsers()
        {
            var users = await _unitOfWork.UserRepository.GetAll().ToListAsync();

            return _mapper.Map<List<UserModel>>(users.OrderBy(x => x.FirstName));
        }
    }
}
