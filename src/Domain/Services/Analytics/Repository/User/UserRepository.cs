﻿using Domain.Services.Analytics.Relational;

namespace Domain.Services.Analytics.Repository.User
{
    public interface IUserRepository : IRepository<Relational.Entities.User>
    {

    }

    public class UserRepository : Repository<Relational.Entities.User>, IUserRepository
    {
        public AnalyticsDbContext Context => DbContext as AnalyticsDbContext;

        public UserRepository(AnalyticsDbContext context) : base(context){}
    }
}
