﻿using System.Threading.Tasks;
using Domain.Services.Analytics.Relational;
using Domain.Services.Analytics.Repository.Delivery;
using Domain.Services.Analytics.Repository.User;
using Microsoft.EntityFrameworkCore;

namespace Domain.Services.Analytics.Repository.UnitOfWork
{
    public interface IUnitOfWork
    {
        IDeliveryRepository DeliveryRepository { get; }
        IUserRepository UserRepository { get; }
        Task<int> CompleteAsync();
    }

    public class UnitOfWork : IUnitOfWork
    {
        private readonly AnalyticsDbContext _context;
        public IUserRepository UserRepository { get; }
        public IDeliveryRepository DeliveryRepository { get; }
      
        public UnitOfWork(AnalyticsDbContext context)
        {
            _context = context;
            _context.Database.Migrate();
            UserRepository = new UserRepository(context);
            DeliveryRepository = new DeliveryRepository(context);
        }

        public async Task<int> CompleteAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}
