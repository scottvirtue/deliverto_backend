﻿using Domain.Services.Analytics.Relational;

namespace Domain.Services.Analytics.Repository.Delivery
{
    public interface IDeliveryRepository : IRepository<Relational.Entities.Delivery>
    {
    }

    public class DeliveryRepository : Repository<Relational.Entities.Delivery>, IDeliveryRepository
    {
        public AnalyticsDbContext Context => DbContext as AnalyticsDbContext;

        public DeliveryRepository(AnalyticsDbContext context) : base(context) {}
    }
}
