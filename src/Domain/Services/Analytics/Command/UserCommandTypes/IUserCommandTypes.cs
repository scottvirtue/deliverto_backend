﻿using Domain.Models;
using Domain.Services.Analytics.Repository.UnitOfWork;

namespace Domain.Services.Analytics.Command.UserCommandTypes
{
    public interface IUserCommandTypes
    {
        bool IsType(string commandType);
        void Execute(IUnitOfWork unitOfWork, UserModel model);
    }
}
