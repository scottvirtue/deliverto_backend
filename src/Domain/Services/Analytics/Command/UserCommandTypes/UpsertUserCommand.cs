﻿using Amazon.DynamoDBv2;
using AutoMapper;
using Domain.Models;
using Domain.Services.Analytics.Relational.Entities;
using Domain.Services.Analytics.Repository.UnitOfWork;

namespace Domain.Services.Analytics.Command.UserCommandTypes
{
    public class UpsertUserCommand : IUserCommandTypes
    {
        private readonly IMapper _mapper;

        public UpsertUserCommand(IMapper mapper)
        {
            _mapper = mapper;
        }

        public bool IsType(string operationType)
        {
            return operationType == OperationType.MODIFY || operationType == OperationType.INSERT;
        }

        public void Execute(IUnitOfWork unitOfWork, UserModel model)
        {
            var dbUser = unitOfWork.UserRepository.Get(model.Username);

            if (dbUser == null)
            {
                unitOfWork.UserRepository.Add(_mapper.Map<User>(model));
            }
            else
            {
                dbUser = _mapper.Map<UserModel, User>(model, dbUser);
            }
        }
    }
}
