﻿using Domain.Models;
using Domain.Services.Analytics.Repository.UnitOfWork;

namespace Domain.Services.Analytics.Command.DeliveryCommandTypes
{
    public interface IDeliveryCommandType
    {
        bool IsType(Amazon.DynamoDBv2.OperationType commandType);
        void Execute(IUnitOfWork unitOfWork, DeliveryModel model);
    }
}
