﻿using Amazon.DynamoDBv2;
using AutoMapper;
using Domain.Models;
using Domain.Services.Analytics.Relational.Entities;
using Domain.Services.Analytics.Repository.UnitOfWork;

namespace Domain.Services.Analytics.Command.DeliveryCommandTypes
{
    public class UpsertDeliveryCommand : IDeliveryCommandType
    {
        private readonly IMapper _mapper;

        public UpsertDeliveryCommand(IMapper mapper)
        {
            _mapper = mapper;
        }

        public bool IsType(OperationType operationType)
        {
            return operationType ==  OperationType.MODIFY || operationType == OperationType.INSERT;
        }

        public void Execute(IUnitOfWork unitOfWork, DeliveryModel model)
        {
            var deliveryToUpdate = unitOfWork.DeliveryRepository.Get(model.Id);

            if (deliveryToUpdate == null)
            {
                unitOfWork.DeliveryRepository.Add(_mapper.Map<Delivery>(model));
            }
            else
            {
                deliveryToUpdate = _mapper.Map<DeliveryModel, Delivery>(model, deliveryToUpdate);
            }
        }
    }
}
