﻿using Domain.Services.Analytics.Relational.Entities;
using Microsoft.EntityFrameworkCore;

namespace Domain.Services.Analytics.Relational
{
    public class AnalyticsDbContext : DbContext
    {
        public AnalyticsDbContext(DbContextOptions<AnalyticsDbContext> options)
            : base(options){ }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseMySQL(ConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Delivery>().HasIndex(u => u.BuyerUsername);
            modelBuilder.Entity<Delivery>().HasIndex(u => u.ReceiverUsername);
        }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Delivery> Deliveries { get; set; }
    }
}
