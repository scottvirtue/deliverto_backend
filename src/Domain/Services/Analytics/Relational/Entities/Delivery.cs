﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Services.Analytics.Relational.Entities
{
    public class Delivery
    {
        [Required]
        [Key]
        [MaxLength(50)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string Id { get; set; }
        public string BuyerUsername { get; set; }
        public string ReceiverUsername { get; set; }
        public string Buyer { get; set; }
        public string Receiver { get; set; }
        public DateTime ExpectedDeliveryDate { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public string MerchantName { get; set; }
        public string TransportProviderName { get; set; }
        public string Products { get; set; }
    }
}
