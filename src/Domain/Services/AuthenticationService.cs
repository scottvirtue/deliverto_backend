﻿using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using Domain.Models;

namespace Domain.Services
{
    public interface IAuthenticationService
    {
        AuthenticatedUserModel GetAuthenticatedUserModel();
    }

    public class AuthenticationService : IAuthenticationService
    {
        private readonly IPrincipal _principal;

        public AuthenticationService(IPrincipal principal)
        {
            _principal = principal;
        }

        public AuthenticatedUserModel GetAuthenticatedUserModel()
        {
            return RetreiveUserFromCurrentPrincipalIdentity();
        }

        public AuthenticatedUserModel RetreiveUserFromCurrentPrincipalIdentity()
        {
            var claimIdentity = (ClaimsIdentity)_principal.Identity;

            var user = new AuthenticatedUserModel()
            {
                Id = claimIdentity.Claims.FirstOrDefault(x => x.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier")?.Value,
                Username = claimIdentity.Claims.FirstOrDefault(x => x.Type == "cognito:username").Value,
                GivenName = claimIdentity.Claims.FirstOrDefault(x => x.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname")?.Value,
                FamilyName = claimIdentity.Claims.FirstOrDefault(x => x.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname")?.Value,
                Email = claimIdentity.Claims.FirstOrDefault(x => x.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress")?.Value,
                PhoneNumber = claimIdentity.Claims.FirstOrDefault(x => x.Type == "phone_number")?.Value,
            };

            return user;
        }
    }
}
