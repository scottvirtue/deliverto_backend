﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.DataAccess;
using Domain.Models;

namespace Domain.Services
{
    public interface IDeliveriesService
    {
        Task<bool> Create(DeliveryModel delivery);
        Task<bool> Update(DeliveryModel delivery);
        Task<List<DeliveryModel>> ListDeliveries();
    }

    public class DeliveriesService : IDeliveriesService
    {
        private readonly IAuthenticationService _authService;
        private readonly IDataAccess<DeliveryModel, DeliveryModel> _deliveryDataAccess;

        public DeliveriesService(IAuthenticationService authService, IDataAccess<DeliveryModel, DeliveryModel> deliveryDataAccess)
        {
            _authService = authService;
            _deliveryDataAccess = deliveryDataAccess;
        }

        public async Task<bool> Create(DeliveryModel delivery)
        {
            if (string.IsNullOrEmpty(delivery.Id))
            {
                delivery.Id = Guid.NewGuid().ToString();
            }

            delivery.CreatedAt = DateTime.UtcNow;
            delivery.ModifiedAt = DateTime.UtcNow;

            await _deliveryDataAccess.Create(delivery);

            return true;
        }

        public async Task<bool> Update(DeliveryModel delivery)
        {
            delivery.ModifiedAt = DateTime.UtcNow;

            await _deliveryDataAccess.Update(delivery);

            return true;
        }

        public async Task<List<DeliveryModel>> ListDeliveries()
        {
            return await _deliveryDataAccess.GetByIndex("BuyerUsername", _authService.GetAuthenticatedUserModel().Username);
        }
    }
}
