﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.DataAccess;
using Domain.Models;

namespace Domain.Services
{
    public interface IUserService
    {
        Task<UserModel> Get();
        Task<List<UserModel>> ListReceivers();
        Task<UserModel> Create(UserModel user);
        Task<UserModel> Update(UserModel user);
    }
    public class UserService : IUserService
    {
        private readonly IAuthenticationService _authService;
        private readonly IDataAccess<UserModel, UserModel> _userDataAccess;

        public UserService(IAuthenticationService authService, IDataAccess<UserModel, UserModel> userDataAccess)
        {
            _authService = authService;
            _userDataAccess = userDataAccess;
        }

        public async Task<UserModel> Get()
        {
            var authUser = _authService.GetAuthenticatedUserModel();

            var user = await _userDataAccess.Get(authUser.Username);

            if (user == null)
            {
                await _userDataAccess.Create(new UserModel
                {
                    Username = authUser.Username,
                    FirstName = authUser.GivenName,
                    LastName = authUser.FamilyName,
                    Email = authUser.Email,
                    PhoneNumber = authUser.PhoneNumber,
                    CreatedAt = DateTime.UtcNow,
                    ModifiedAt = DateTime.UtcNow,
                    Type = UserType.Buyer.ToString()
                });

                user = await _userDataAccess.Get(authUser.Username);
            }

            return user;
        }

        public async Task<List<UserModel>> ListReceivers()
        {
            return await _userDataAccess.GetByIndex("Type", UserType.Receiver.ToString());
        }

        public async Task<UserModel> Create(UserModel user)
        {
            if (string.IsNullOrEmpty(user.Username))
            {
                user.Username = Guid.NewGuid().ToString();
            }

            user.CreatedAt = DateTime.UtcNow;
            user.ModifiedAt = DateTime.UtcNow;

            await _userDataAccess.Create(user);

            return await _userDataAccess.Get(user.Username);
        }

        public async Task<UserModel> Update(UserModel user)
        {
            user.ModifiedAt = DateTime.UtcNow;

            await _userDataAccess.Update(user);

            return await _userDataAccess.Get(user.Username);
        }
    }
}
