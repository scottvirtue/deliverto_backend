﻿namespace Domain.Models
{
    public class DeliveryEvent
    {
        public DeliveryEventType EventType { get; set; }
        public DeliveryModel Model { get; set; }
    }

    public enum DeliveryEventType
    {
        Create = 0,
        Update = 1
    }
}
