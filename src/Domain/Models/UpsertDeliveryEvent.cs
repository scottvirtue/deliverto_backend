﻿using Amazon.DynamoDBv2;

namespace Domain.Models
{
    public class UpsertDeliveryEvent
    {
        public OperationType CommandType { get; set; }
        public DeliveryModel Model { get; set; }
    }
}
