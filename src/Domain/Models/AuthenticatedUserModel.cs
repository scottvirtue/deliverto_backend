﻿namespace Domain.Models
{
    public class AuthenticatedUserModel
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string GivenName { get; set; }
        public string FamilyName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
    }
}
