﻿using Amazon.DynamoDBv2;

namespace Domain.Models
{
    public class UpsertUserEvent
    {
        public OperationType CommandType { get; set; }
        public UserModel Model { get; set; }
    }
}
