﻿using System;
using Domain.DataAccess.Models;

namespace Domain.Models
{
    public class DeliveryModel
    {
        [AttributeNotExistsOnPut]
        [AttributeExistsOnUpdate]
        public string Id { get; set; }
        public string BuyerUsername { get; set; }
        public string ReceiverUsername { get; set; }
        public UserModel Buyer { get; set; }
        public UserModel Receiver { get; set; }
        public DateTime ExpectedDeliveryDate { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public string MerchantName { get; set; }
        public string TransportProviderName { get; set; }
        public string Products { get; set; }
        public DateTime? ModifiedAt { get; set; }
        public DateTime? CreatedAt { get; set; }
    }
}
