﻿namespace Domain.Models
{
    public enum UserType
    {
        Buyer = 0,
        Receiver = 1
    }
}
