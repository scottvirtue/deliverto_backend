﻿using System;
using Domain.DataAccess.Models;

namespace Domain.Models
{
    public class UserModel
    {
        [AttributeNotExistsOnPut]
        [AttributeExistsOnUpdate]
        public string Username { get; set; }
        public Location Location { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? ModifiedAt { get; set; }
        public DateTime? CreatedAt { get; set; }
        public string Type { get; set; }
    }
}
