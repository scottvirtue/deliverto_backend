﻿using Amazon.Lambda.Core;
using Amazon.Lambda.DynamoDBEvents;
using Domain.IoC;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Threading.Tasks;
using Domain.Services.Analytics;
using Microsoft.Extensions.Configuration;

[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]
namespace Events
{
    public class AnalyticsDeliveriesTrigger
    {
        private ILambdaContext _context;
        private IConfiguration _configuration;
        private IConfiguration Configuration => _configuration ?? (_configuration = InitConfiguration());

        private IAnalyticsService Service => InitialiseIoC().GetService<IAnalyticsService>();

        public async Task<string> FunctionHandler(DynamoDBEvent evnt, ILambdaContext context)
        {
            _context = context;

            context.Logger.LogLine($"{nameof(AnalyticsDeliveriesTrigger)} Triggered -> {JsonConvert.SerializeObject(evnt)}");

            var result = false;

            try
            {
                result = await Service.UpsertDeliveries(evnt);
            }
            catch (Exception ex)
            {
                context.Logger.LogLine($"ERROR:{ex.Message}");
            }

            context.Logger.LogLine($"Result of {nameof(AnalyticsDeliveriesTrigger)} : {result.ToString()}");

            return result.ToString();
        }

        public ServiceProvider InitialiseIoC()
        {
            _context.Logger.LogLine($"Initializing IoC");

            var services = new ServiceCollection();

            services.ConfigureIoC(Configuration, _context);

            var serviceProvider = services.BuildServiceProvider();

            _context.Logger.LogLine($"Completed Initializing IoC");

            return serviceProvider;
        }

        private IConfiguration InitConfiguration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddEnvironmentVariables()
                .Build();
        }
    }
}
