﻿using Amazon.Lambda.Core;
using Amazon.Lambda.SNSEvents;
using Domain.IoC;
using Domain.Models;
using Domain.Services;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace Events
{
    public class DeliveriesTrigger
    {
        private ILambdaContext _context;
        private IConfiguration _configuration;
        private IConfiguration Configuration => _configuration ?? (_configuration = InitConfiguration());

        private IDeliveriesService Service => InitialiseIoC().GetService<IDeliveriesService>();

        public async Task<string> FunctionHandler(SNSEvent evnt, ILambdaContext context)
        {
            _context = context;

            context.Logger.LogLine($"{nameof(DeliveriesTrigger)} Triggered -> {JsonConvert.SerializeObject(evnt)}");

            var result = false;

            try
            {
                var @event = JsonConvert.DeserializeObject<DeliveryEvent>(evnt.Records.FirstOrDefault()?.Sns.Message);

                switch (@event.EventType)
                {
                    case DeliveryEventType.Create:
                        result = await Service.Create(@event.Model);
                        break;
                    case DeliveryEventType.Update:
                        result = await Service.Update(@event.Model);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

            }
            catch (Exception ex)
            {
                context.Logger.LogLine($"ERROR:{ex.Message}, {ex.StackTrace}, {ex.InnerException}");
            }
           
            context.Logger.LogLine($"Result of {nameof(DeliveriesTrigger)} : {result.ToString()}");

            return result.ToString();
        }

        public ServiceProvider InitialiseIoC()
        {
            _context.Logger.LogLine($"Initializing IoC");

            var services = new ServiceCollection();

            services.ConfigureIoC(Configuration, _context);

            var serviceProvider = services.BuildServiceProvider();

            _context.Logger.LogLine($"Completed Initializing IoC");

            return serviceProvider;
        }

        private IConfiguration InitConfiguration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddEnvironmentVariables()
                .Build();
        }
    }
}
