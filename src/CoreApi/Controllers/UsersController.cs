using System.Linq;
using Domain;
using Domain.Models;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Domain.Services;

namespace CoreApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var result = await _userService.Get();

            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Post(UserModel model)
        {
            var result = await _userService.Create(model);

            return Ok(result);
        }

        [HttpPut]
        public async Task<IActionResult> Put(UserModel model)
        {
            var result = await _userService.Update(model);

            return Ok(result);
        }

        [HttpGet("receivers/list")]
        public async Task<IActionResult> ListReceivers()
        {
            var result = (await _userService.ListReceivers()).OrderBy(x => x.FirstName);

            return Ok(result);
        }
    }
}
