using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;
using Domain.Models;
using Domain.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace CoreApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DeliveriesController : ControllerBase
    {
        private readonly IAmazonSimpleNotificationService _snsService;
        private readonly IConfiguration _configuration;
        private readonly IDeliveriesService _deliveriesService;

        public DeliveriesController(IAmazonSimpleNotificationService snsService, IConfiguration configuration, IDeliveriesService deliveriesService)
        {
            _snsService = snsService;
            _configuration = configuration;
            _deliveriesService = deliveriesService;
        }

        [HttpPost]
        public async Task<IActionResult> Post(DeliveryModel model)
        {
            var response = await _snsService.PublishAsync(new PublishRequest
            {
                TopicArn = _configuration["DeliveriesTriggerSNSTopic"],
                Message = JsonConvert.SerializeObject(new DeliveryEvent
                {
                    EventType = DeliveryEventType.Create,
                    Model = model
                }),
                MessageStructure = "RAW"
            });

            if (response.HttpStatusCode != HttpStatusCode.OK) throw new Exception($"Failed To Publish - SNS Response Code {response.HttpStatusCode}");

            return Ok($"MessageId: {response.MessageId}");
        }

        [HttpPut]
        public async Task<IActionResult> Put(DeliveryModel model)
        {
            var response = await _snsService.PublishAsync(new PublishRequest
            {
                TopicArn = _configuration["DeliveriesTriggerSNSTopic"],
                Message = JsonConvert.SerializeObject(new DeliveryEvent
                {
                    EventType = DeliveryEventType.Update,
                    Model = model
                }),
                MessageStructure = "RAW"
            });

            if (response.HttpStatusCode != HttpStatusCode.OK) throw new Exception($"Failed To Publish - SNS Response Code {response.HttpStatusCode}");

            return Ok($"MessageId: {response.MessageId}");
        }

        [HttpGet("list")]
        public async Task<IActionResult> ListDeliveries()
        {
            var result = (await _deliveriesService.ListDeliveries()).OrderByDescending(x => x.DeliveryDate);

            return Ok(result);
        }
    }
}
