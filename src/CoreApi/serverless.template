{
  "AWSTemplateFormatVersion": "2010-09-09",
  "Transform": "AWS::Serverless-2016-10-31",
  "Description": "An AWS Serverless Application that uses the ASP.NET Core framework running in Amazon Lambda.",

  "Parameters": {
    "PoolClientId": {
      "Type": "String",
      "Description": "",
      "MinLength": "0"
    },
	 "PoolAuthorityUrl": {
      "Type": "String",
      "Description": "",
      "MinLength": "0"
    },
	"AnalyticsDatabaseConnectionString": {
      "Type": "String",
      "Description": "",
      "MinLength": "0"
    }
  },
  "Conditions": {
    
  },

  "Resources": {
    "AspNetCoreFunction": {
      "Type": "AWS::Serverless::Function",
      "Properties": {
        "Handler": "CoreApi::CoreApi.LambdaEntryPoint::FunctionHandlerAsync",
        "Runtime": "dotnetcore2.1",
        "CodeUri": "",
        "MemorySize": 512,
        "Timeout": 30,
        "Role": null,
        "Policies": [
          "AWSLambdaFullAccess"
        ],
        "Environment": {
          "Variables": {
		   "PoolClientId": { "Ref" : "PoolClientId" },
		   "PoolAuthorityUrl":  { "Ref" : "PoolAuthorityUrl" },
		   "UsersTable": { "Ref" : "UsersTable" },
		   "DeliveriesTable": { "Ref" : "DeliveriesTable" },
		   "DeliveriesTriggerSNSTopic" : {"Ref":"DeliveriesTriggerSNSTopic"}
          }
        },
        "Events": {
          "ProxyResource": {
            "Type": "Api",
            "Properties": {
              "Path": "/{proxy+}",
              "Method": "ANY"
            }
          },
          "RootResource": {
            "Type": "Api",
            "Properties": {
              "Path": "/",
              "Method": "ANY"
            }
          }
        }
      }
    },
	"UsersTable":{
		"Type" : "AWS::DynamoDB::Table",
		"DeletionPolicy":"Retain",
		"Properties" : {
			"AttributeDefinitions" : [
				{
				  "AttributeName" : "Username",
				  "AttributeType" : "S"
				},
				{
				  "AttributeName" : "Type",
				  "AttributeType" : "S"
				}
			],
			"KeySchema" : [ 
				{
				  "AttributeName" : "Username",
				  "KeyType" : "HASH"
				}
			],
			"GlobalSecondaryIndexes" : [
				{
				  "IndexName" : "ByType",
				  "KeySchema" : [ 
				    {
					  "AttributeName" : "Type",
					  "KeyType" : "HASH"
					},
					{
					  "AttributeName" : "Username",
					  "KeyType" : "RANGE"
					}
					],
				  "Projection" : { "ProjectionType" : "ALL"  }
				}				
			],
			"StreamSpecification":{
				"StreamViewType" : "NEW_IMAGE"
			},
			"LocalSecondaryIndexes" : [  ],
			"PointInTimeRecoverySpecification" : {"PointInTimeRecoveryEnabled" : "true"},
			"BillingMode":"PAY_PER_REQUEST",
			"SSESpecification" : {"SSEEnabled" : true},
			"TableName" :{"Fn::Sub": "DeliverTo_Users" } 
		}
	},
	"DeliveriesTable":{
		"Type" : "AWS::DynamoDB::Table",
		"DeletionPolicy":"Retain",
		"Properties" : {
			"AttributeDefinitions" : [
				{
				  "AttributeName" : "Id",
				  "AttributeType" : "S"
				},
				{
				  "AttributeName" : "BuyerUsername",
				  "AttributeType" : "S"
				},
				{
				  "AttributeName" : "ReceiverUsername",
				  "AttributeType" : "S"
				}
			],
			"KeySchema" : [ 
				{
				  "AttributeName" : "Id",
				  "KeyType" : "HASH"
				}
			],
			"GlobalSecondaryIndexes" : [
				{
				  "IndexName" : "ByBuyerUsername",
				  "KeySchema" : [ 
				    {
					  "AttributeName" : "BuyerUsername",
					  "KeyType" : "HASH"
					},
					{
					  "AttributeName" : "Id",
					  "KeyType" : "RANGE"
					}
					],
				  "Projection" : { "ProjectionType" : "ALL"  }
				},
				{
				  "IndexName" : "ByReceiverUsername",
				  "KeySchema" : [ 
				    {
					  "AttributeName" : "ReceiverUsername",
					  "KeyType" : "HASH"
					},
					{
					  "AttributeName" : "Id",
					  "KeyType" : "RANGE"
					}
					],
				  "Projection" : { "ProjectionType" : "ALL"  }
				}
			],
			"StreamSpecification":{
				"StreamViewType" : "NEW_IMAGE"
			},
			"LocalSecondaryIndexes" : [  ],
			"PointInTimeRecoverySpecification" : {"PointInTimeRecoveryEnabled" : "true"},
			"BillingMode":"PAY_PER_REQUEST",
			"SSESpecification" : {"SSEEnabled" : true},
			"TableName" :{"Fn::Sub": "DeliverTo_Deliveries" } 
		}
	},
	"AnalyticsUsersTrigger" : {
      "Type" : "AWS::Serverless::Function",
      "Properties": {
        "Handler": "Events::Events.AnalyticsUsersTrigger::FunctionHandler",
        "Runtime": "dotnetcore2.1",
        "CodeUri": "",
        "MemorySize": 512,
        "Timeout": 30,
        "Role": null,
        "Policies": [ "AWSLambdaFullAccess" ],
		"Environment" : {
          "Variables" : {
			  "AnalyticsDatabaseConnectionString": { "Ref" : "AnalyticsDatabaseConnectionString" }
          }
        }, 
		"Events": {
          "DynamoUpsertTrigger": {
            "Type": "DynamoDB",
            "Properties": {
              "Stream" : { "Fn::GetAtt" : [ "UsersTable", "StreamArn" ] },
			  "StartingPosition" : "TRIM_HORIZON",
              "BatchSize" : 10
            }
          }
        }
      }
    },
	"AnalyticsDeliveriesTrigger" : {
      "Type" : "AWS::Serverless::Function",
      "Properties": {
        "Handler": "Events::Events.AnalyticsDeliveriesTrigger::FunctionHandler",
        "Runtime": "dotnetcore2.1",
        "CodeUri": "",
        "MemorySize": 512,
        "Timeout": 30,
        "Role": null,
        "Policies": [ "AWSLambdaFullAccess" ],
		"Environment" : {
          "Variables" : {
			 "AnalyticsDatabaseConnectionString": { "Ref" : "AnalyticsDatabaseConnectionString" }
          }
        }, 
		"Events": {
          "DynamoUpsertTrigger": {
            "Type": "DynamoDB",
            "Properties": {
              "Stream" : { "Fn::GetAtt" : [ "DeliveriesTable", "StreamArn" ] },
			  "StartingPosition" : "TRIM_HORIZON",
              "BatchSize" : 10
            }
          }
        }
      }
    },
	"DeliveriesTriggerFunction" : {
      "Type" : "AWS::Serverless::Function",
      "Properties": {
        "Handler": "Events::Events.DeliveriesTrigger::FunctionHandler",
        "Runtime": "dotnetcore2.1",
        "CodeUri": "",
        "MemorySize": 512,
        "Timeout": 30,
        "Role": null,
        "Policies": [ "AWSLambdaFullAccess" ],
		"Environment" : {
          "Variables" : {
			"DeliveriesTable" : { "Ref" : "DeliveriesTable" }
          }
        },
		"Events": {
          "SNSTrigger": {
            "Type": "SNS",
            "Properties": {
              "Topic" : {"Ref":"DeliveriesTriggerSNSTopic"}
            }
          }
        }
      }
    },
	"DeliveriesTriggerSNSTopic" : {
	   "Type" : "AWS::SNS::Topic",
	   "Properties" : {
		  "TopicName" : "DeliveriesTriggerSNSTopic" 
	   }
	}
  },

  "Outputs": {
    "DeliverToCoreApiURL": {
      "Description": "API endpoint URL for Prod environment",
      "Value": {
        "Fn::Sub": "https://${ServerlessRestApi}.execute-api.${AWS::Region}.amazonaws.com/Prod/"
      }
    }
  }
}